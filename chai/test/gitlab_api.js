var chai = require( 'chai' )
  , chaiHttp = require( 'chai-http' )
  , assert = chai.assert
  , expect = chai.expect
  , should = chai.should
  , projectId = '12937917'
  , url = 'https://gitlab.com/api/v4/projects/' + projectId + '/issues'
  , privateToken = 'vj4E5FSVsCWmDcANoUTb';

chai.use( chaiHttp );

describe( 'Gitlab Issues API', function(){

  this.timeout( 4000 );

  it( 'can be created and removed', function( done ){

    var timestamp = (new Date).getTime()
      , payload = {
          'title': 'My new issue' + timestamp
        , 'description': 'I was created by automated tests!'
        , 'labels': 'automation,rules'
        };

    chai.request( url )
        .post( '/' )
        .set( 'PRIVATE-TOKEN', privateToken )
        .query( payload )
        .end(function( err, res ){
          expect( res ).to.have.status( 201 );

          expect( res.body.title ).to.equal( payload['title'] );
          expect( res.body.description ).to.equal( payload['description'] );
          assert.deepEqual( res.body.labels, ['automation', 'rules'] );

          chai.request( url )
              .delete( '/' + res.body.iid )
              .set( 'PRIVATE-TOKEN', privateToken )
              .end(function( err, res ){
                expect( res ).to.have.status( 204 );

                done();
              });


    });
  });

  it( 'can update an existing issue', function( done ){
    chai.request( url )
        .get( '/1' )
        .end(function( err, originalResponse ){
            var timestamp = (new Date).getTime();

            update({
              'description': originalResponse.body.description + '\n\n' + timestamp
            }, function( err, res ){

              expect(res).to.have.status( 200 );
              expect( res.body.description ).to.not.equal( originalResponse.body.description )
              expect( res.body.description ).to.include( 'This issue is used as static test data' )
              expect( res.body.description ).to.include( timestamp )

              done();
            });
        });
  });

  describe( 'updating error scenarios', function(){
    it( 'fails with invalid fields', function( done ){
      update( {'bogusfield': 'invalid'}, function( err, res ){
          expect(res).to.have.status( 400 );
          done();
      });
    });

    it( 'checks type for field "confidential"', function( done ){
      update( {'confidential': 'invalidvalue'}, function( err, res ){
        expect(res).to.have.status( 400 );
        done();
      });
    });

    it( 'checks type for field "weight"', function( done ){
      update( {'weight': 'invalidvalue'}, function( err, res ){
        expect(res).to.have.status( 400 );
        done();
      });
    });

  });



});

function update( data, verification_callback ){
  chai.request( url )
      .put( '/1' )
      .set( 'PRIVATE-TOKEN', privateToken )
      .query( data )
      .end( verification_callback );
};
