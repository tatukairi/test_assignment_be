*** Settings ***
Documentation    Getting single issue is not separately tested, as other test
...              cases use that functionality -- if it's broken, the test cases
...              here will fail.
...
...              Notes: Due to time constraints, I went with directly using
...              Pythons requests module[1]. For real, I should probably use
...              proper HTTPLibrary[2] or push the dev team to move towards
...              contract-based API development[3] with a library[4] appropriate
...              for that task
...
...              [1] https://2.python-requests.org/
...
...              [2] https://github.com/bulkan/robotframework-requests/#readme
...
...              [3] https://www.quora.com/What-is-an-API-contract-and-who-should-create-it-when-building-a-set-of-APIs
...
...              [4] https://github.com/asyrjasalo/RESTinstance/

Library    Collections
Library    DateTime

*** Variables ***
# can be overridden from command line, thus these tests can be run against any project in Gitlab
${PROJECT ID}=    12937917
${URL}=    https://gitlab.com/api/v4/projects/${PROJECT ID}/issues

# normally these kinds of things should not be hard-coded but provided through CI securely
${ACCESS TOKEN}=   vj4E5FSVsCWmDcANoUTb
&{DEFAULT HEADERS}=    PRIVATE-TOKEN=${ACCESS TOKEN}

@{LABELS}=     automation    rules

*** Test cases ***

Creating and removing an issue should succeed
    [Documentation]    I decided to combine creating and deleting to one test case as
    ...                Deletion requires one to first create an issue and on
    ...                the other hand, Creation without cleaning up (ie. Deletion)
    ...                will just mess up known starting state of the System Under Test.
    ${timestamp}=    Get current timestamp   # title cannot be identical

    &{response}=    Create issue    title=My new issue ${timestamp}
    ...                             description=I was created by automated tests!
    ...                             labels=automation,rules
    Validate response    ${response}
    ...                  title=My new issue ${timestamp}
    ...                  description=I was created by automated tests!
    ...                  labels=${labels}

    Delete issue    ${response}[iid]
    @{issues}=    List all open issues
    Length should be    ${issues}    1

Updating an existing issue should succeed
    ${timestamp}=     Get current timestamp
    &{original response}=    Get issue    1

    Update issue    1    description=${original response}[description]${\n * 2}${timestamp}   # edit description by adding a new timestamp

    &{response}=    Get issue    1
    Should not be equal as strings    ${response}[description]    ${original response}[description]
    Should contain    ${response}[description]    This issue is used as static test data
    Should contain    ${response}[description]    ${timestamp}

Updating invalid fields should fail
    [Documentation]    This is an example of data-driven[1] test case which verifies that multiple invalid edge cases fail as expected.
    ...                [1] http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#data-driven-style
    [Template]    Updating invalid field in an issue should fail
    weight=invalid value
    confidential=invalid value
    nonexistent-field=and invalid value

*** Keywords ***
Create issue
    [Arguments]    &{payload}
    ${response}=    Evaluate    requests.post('${URL}', headers=&{DEFAULT HEADERS}, params=&{payload})    requests
    Should be equal as integers    ${response.status_code}    201
    [Return]    ${response.json()}
    [Teardown]    Run keyword if    '${KEYWORD STATUS}' == 'FAIL'    Log    ${response.json()}

Validate response
    [Arguments]    ${response}    &{validations}
    FOR    ${v}    IN    @{validations.items()}
         ${expected key}    ${expected value}=    Set variable    ${v}
         Dictionary should contain item    ${response}    ${expected key}    ${expected value}
    END

Delete issue
    [Arguments]    ${id}
    ${response}=    Evaluate    requests.delete('${URL}/${id}', headers=&{DEFAULT HEADERS})    requests
    Should be equal as integers    ${response.status_code}    204
    [Teardown]    Run keyword if    '${KEYWORD STATUS}' == 'FAIL'    Log    ${response.json()}

List all open issues
    ${response}=    Evaluate    requests.get('${URL}?state=opened', headers=&{DEFAULT HEADERS})    requests
    Should be equal as integers    ${response.status_code}    200
    [Return]    ${response.json()}

Get issue
    [Arguments]    ${id}
    ${response}=    Evaluate    requests.get('${URL}/${id}', headers=&{DEFAULT HEADERS})    requests
    Should be equal as integers    ${response.status_code}    200
    [Return]    ${response.json()}

Get current timestamp
    ${d}=    Get current date    result_format=epoch
    Run keyword and return    Convert to string    ${d}

Update issue
    [Arguments]    ${id}   &{payload}
    ${response}=    Evaluate    requests.put('${URL}/${id}', headers=&{DEFAULT HEADERS}, params=&{payload})    requests
    Should be equal as integers    ${response.status_code}    200
    [Return]     ${response.json()}

Updating invalid field in an issue should fail
    [Arguments]    &{field and value}
    Run keyword and expect error    400 != 200    Update issue    1    &{field and value}